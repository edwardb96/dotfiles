#/bin/bash

if [[ $1 == "preview" ]]; then
    echo "Previewing Install..."
    preview=true
else
    echo "Installing..."
    preview=false
fi

find . -name '*.symlink*' | while read line; do
    directoryName=$(dirname "$line")
    directoryNameWithoutLeader=${directoryName:2}

    fileNameWithExtension=$(basename "$line")
    fileNameWithoutExtension="${fileNameWithExtension/.symlink/}"
    echo $fileNameWithoutExtension

    if [[ ! -z $directoryNameWithoutLeader ]]; then
        name="$HOME/.$directoryNameWithoutLeader/$fileNameWithoutExtension"
    else
        name="$HOME/.$fileNameWithoutExtension"
    fi

    target=$(readlink -f $line)
    if [[ -d $target ]]; then
	target="$target/"	
    fi
    
    if [[ $preview == true ]]; then
        echo "ln -sfT $target $name"
    else
        ln -sfT "$target" "$name"
    fi
done 


