atom.commands.add 'atom-text-editor', 'custom-core:save-and-close', ->
  editor = atom.workspace.getActiveTextEditor()
  editor.save().then ->
    editor.destroy()

atom.commands.add 'atom-text-editor', 'custom-core:force-close', ->
  editor = atom.workspace.getActiveTextEditor()
  editor.destroy()
