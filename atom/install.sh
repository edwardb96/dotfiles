#/bin/bash
apm install linter &&
apm install vim-mode-plus &&
apm install linter-clang &&
apm install autocomplete-clang &&
apm install clang-format &&
apm install platformio-ide-terminal &&
apm install language-cmake &&
apm install autocomplete-cmake 
